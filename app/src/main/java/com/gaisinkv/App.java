package com.gaisinkv;

import com.gaisinkv.pivotStrategies.PivotStrategies;
import com.gaisinkv.sortStrategies.SortStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Класс-runner
 */
public class App {

    private final static Logger LOGGER = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        String[] letters = {"cc", "bb", "zz", "abc", "aa", "xxx", "xxa", "zaz"};
        List<String> lettersList = Arrays.asList(letters);

        Sorter<String> sorter = SortBuilder.newBuilder()
                .sortStrategy(SortStrategies.QUICK)
                .pivotStrategy(PivotStrategies.MIDDLE_ELEMENT)
                .build(new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2) {
                        if (o1.compareTo(o2) == 0) return 0;
                        return o1.compareTo(o2) > 0 ? 1 : -1;
                    }
                });

        LOGGER.info("Исходный список: " + lettersList);
        sorter.sort(lettersList);
        LOGGER.info("Результат сортировки: " + lettersList);
    }
}
