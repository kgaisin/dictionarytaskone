package com.gaisinkv;

import com.gaisinkv.sortStrategies.SortStrategy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Класс сортировки, не меняющий исходный массив в ходе сортировки
 */
public class ImmutableSorter<T> extends ParentSorter<T> {
    public ImmutableSorter(SortStrategy<T> sortStrategy, Comparator<T> comparator) {
        super(sortStrategy, comparator);
    }

    public List<T> sort(List<T> list) {
        List<T> listToSort = new ArrayList<>(list);
        return super.sort(listToSort);
    }
}
