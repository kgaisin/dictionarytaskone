package com.gaisinkv;

import com.gaisinkv.pivotStrategies.*;
import com.gaisinkv.sortStrategies.BubbleSortStrategy;
import com.gaisinkv.sortStrategies.QuickSortStrategy;
import com.gaisinkv.sortStrategies.SortStrategies;
import com.gaisinkv.sortStrategies.SortStrategy;

/**
 * Фабрика стратегий сортировки и выбора опорного элемента
 */
public class SortStrategyFactory {
    public static <T> SortStrategy<T> getSortStrategy(SortStrategies strategy, PivotStrategies pivot) {
        if(strategy == null)
            strategy = SortStrategies.DEFAULT;

        SortStrategy<T> sortStrategy;
        switch (strategy) {
            case DEFAULT:
            case QUICK:
                sortStrategy = new QuickSortStrategy<>(SortStrategyFactory.getPivotStrategy(pivot));
                break;
            case BUBBLE:
                sortStrategy = new BubbleSortStrategy<>();
                break;
            default:
                throw new RuntimeException("Неизвестный сортировщик");
        }
        return sortStrategy;
    }

    private static PivotStrategy getPivotStrategy(PivotStrategies pivotStrategy) {
        if(pivotStrategy == null)
            pivotStrategy = PivotStrategies.MIDDLE_ELEMENT;

        PivotStrategy pivot;
        switch (pivotStrategy) {
            case FIRST_ELEMENT:
                pivot = new FirstElementPivotStrategy();
                break;
            case MIDDLE_ELEMENT:
                pivot = new MiddleElementPivotStrategy();
                break;
            case LAST_ELEMENT:
                pivot = new LastElementPivotStrategy();
                break;
            default:
                throw new RuntimeException("Неизвестная стратегия выбора опорного элемента");
        }
        return pivot;
    }
}
