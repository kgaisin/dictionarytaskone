package com.gaisinkv;

import com.gaisinkv.sortStrategies.SortStrategy;

import java.util.Comparator;
import java.util.List;

public class MutableSorter<T> extends ParentSorter<T> {
    public MutableSorter(SortStrategy<T> sortStrategy, Comparator<T> comparator) {
        super(sortStrategy, comparator);
    }

    public List<T> sort(List<T> list) {
        return super.sort(list);
    }
}
