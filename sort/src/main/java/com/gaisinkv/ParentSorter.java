package com.gaisinkv;

import com.gaisinkv.sortStrategies.SortStrategy;

import java.util.Comparator;
import java.util.List;

/**
 * Базовый класс сортировки
 */
public class ParentSorter<T> implements Sorter<T> {
    private SortStrategy<T> sortStrategy;
    private Comparator<T> comparator;

    ParentSorter(SortStrategy<T> sortStrategy, Comparator<T> comparator) {
        this.sortStrategy = sortStrategy;
        this.comparator = comparator;
    }

    public List<T> sort(List<T> list) {
        sortStrategy.sort(list, comparator);
        return list;
    }
}
