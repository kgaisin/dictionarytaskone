package com.gaisinkv.exceptions;

public class ArrayIsEmptyException extends RuntimeException {
    public ArrayIsEmptyException(String message) {
        super(message);
    }
}
