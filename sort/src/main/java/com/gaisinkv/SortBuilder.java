package com.gaisinkv;

import com.gaisinkv.pivotStrategies.PivotStrategies;
import com.gaisinkv.sortStrategies.SortStrategies;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Класс-конструктор сортировщика массива
 */
public class SortBuilder {

    private SortStrategies sortStrategy;
    private PivotStrategies pivotStrategy;

    private SortBuilder() {
    }

    public static SortBuilder newBuilder() {
        return new SortBuilder();
    }

    public SortBuilder sortStrategy(SortStrategies sortStrategy) {
        this.sortStrategy = sortStrategy;
        return this;
    }

    public SortBuilder pivotStrategy(PivotStrategies pivotStrategy) {
        this.pivotStrategy = pivotStrategy;
        return this;
    }

    public <T> Sorter<T> build(Comparator<T> comparator) {
        return new MutableSorter<T>(SortStrategyFactory.<T>getSortStrategy(sortStrategy, pivotStrategy), comparator);
    }

    public <T> Sorter<T> buildImmutable(Comparator<T> comparator) {
        return new ImmutableSorter<T>(SortStrategyFactory.<T>getSortStrategy(sortStrategy, pivotStrategy), comparator) {
            @Override
            public List<T> sort(List<T> list) {
                List<T> finalList = super.sort(list);
                return Collections.unmodifiableList(finalList);
            }
        };
    }
}
