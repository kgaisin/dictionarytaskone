package com.gaisinkv.sortStrategies;

import com.gaisinkv.exceptions.ArrayIsEmptyException;
import com.gaisinkv.pivotStrategies.MiddleElementPivotStrategy;
import com.gaisinkv.pivotStrategies.PivotStrategy;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * Сортировщик методом быстрой сортировки
 */
public class QuickSortStrategy<T> implements SortStrategy<T> {

    private final PivotStrategy pivotStrategy;

    public QuickSortStrategy(PivotStrategy pivotStrategy) {
        this.pivotStrategy = pivotStrategy == null ? new MiddleElementPivotStrategy() : pivotStrategy;
    }

    /**
     * Метод быстрой лексикографической сортировки массива
     * @param list - массив строк, которые требуется отсортировать
     */
    public void sort(List<T> list, Comparator<T> comparator) {

        Objects.requireNonNull(list, "array is required!");

        int startIndex = 0;
        int endIndex = list.size() - 1;

        if (list.size() == 0)
            throw new ArrayIsEmptyException("Пустой массив!");
        quickSort(list, startIndex, endIndex, comparator);
    }

    private void quickSort(List<T> list, int startIndex, int endIndex, Comparator<T> comparator) {

        if (startIndex >= endIndex)
            return;

        int i = startIndex, j = endIndex;
        int pivot = pivotStrategy.getPivot(i, j);

        while (i < j) {
            while (i < pivot && (comparator.compare(list.get(i), list.get(pivot)) <= 0)) {
                i++;
            }
            while (j > pivot && (comparator.compare(list.get(pivot),list.get(j)) <= 0)) {
                j--;
            }
            if (i < j) {
                T temp = list.get(i);
                list.set(i, list.get(j));
                list.set(j, temp);
                if (i == pivot)
                    pivot = j;
                else if (j == pivot)
                    pivot = i;
            }
        }
        quickSort(list, startIndex, pivot, comparator);
        quickSort(list, pivot + 1, endIndex, comparator);
    }
}
