package com.gaisinkv.sortStrategies;

import com.gaisinkv.exceptions.ArrayIsEmptyException;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * Сортировщик пузырьком
 */
public class BubbleSortStrategy<T> implements SortStrategy<T> {

    /**
     * Метод лексикографической сортировки массива строк пузырьком
     * @param list - массив строк, которые требуется отсортировать
     */
    public void sort(List<T> list, Comparator<T> comparator) {

        Objects.requireNonNull(list, "list is required!");

        if(list.size()==0)
            throw new ArrayIsEmptyException("Пустой список!");

        for (int i = 0; i < list.size(); i++) {
            T current = list.get(i);

            for (int j = i - 1; j >= 0; j--) {
                T left = list.get(j);

                if (comparator.compare(current, left) < 0) {
                    list.set(j+1, left);
                    list.set(j ,current);
                } else {
                    break;
                }
            }
        }
    }
}
