package com.gaisinkv.sortStrategies;

public enum SortStrategies {
    DEFAULT, BUBBLE, QUICK
}
