package com.gaisinkv.pivotStrategies;

/**
 * Стратегия выбора опорного элемента по среднему элементу массива
 */
public class MiddleElementPivotStrategy implements PivotStrategy {
    public int getPivot(int startIndex, int endIndex) {
        return startIndex - (startIndex - endIndex) /2;
    }
}
