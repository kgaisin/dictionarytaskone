package com.gaisinkv.pivotStrategies;

/**
 * Стратегия выбора опорного элемента по последнему элементу массива
 */
public class LastElementPivotStrategy implements PivotStrategy {
    public int getPivot(int startIndex, int endIndex) {
        return endIndex;
    }
}
