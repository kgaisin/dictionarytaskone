package com.gaisinkv.pivotStrategies;

/**
 * Стратегия реализации опорного элемента для алгоритма быстрой сортировки
 */
public interface PivotStrategy {
    int getPivot(int startIndex, int endIndex);
}
