package com.gaisinkv.pivotStrategies;

/**
 * Стратегия выбора опорного элемента по первому элементу массива
 */
public class FirstElementPivotStrategy implements PivotStrategy {
    public int getPivot(int startIndex, int endIndex) {
        return startIndex;
    }
}
