package com.gaisinkv.pivotStrategies;

public enum PivotStrategies {
    FIRST_ELEMENT, MIDDLE_ELEMENT, LAST_ELEMENT
}
