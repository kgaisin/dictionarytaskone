package com.gaisinkv;

import com.gaisinkv.pivotStrategies.PivotStrategies;
import com.gaisinkv.sortStrategies.SortStrategies;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.*;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class SortBuilderTest {

    private String[] unsorted = {"c", "a", "b"};
    private List<String> unsortedList = Arrays.asList(unsorted);
    private String[] sorted = Arrays.copyOf(unsorted, unsorted.length);
    private List<String> sortedList = Arrays.asList(sorted);
    { Collections.sort(sortedList); }

    @Test
    public void testNewBuilder() {
        assertNotNull(SortBuilder.newBuilder());
    }

    @Test
    public void testSortStrategy() {
        SortBuilder testBuilder = SortBuilder.newBuilder()
                .sortStrategy(SortStrategies.QUICK);
        assertNotNull(testBuilder);
    }

    @Test
    public void testPivotStrategy() {
        SortBuilder testBuilder = SortBuilder.newBuilder()
                .pivotStrategy(PivotStrategies.MIDDLE_ELEMENT);
        assertNotNull(testBuilder);
    }

    @Test
    public void testBuild() {
        Sorter<String> testSorter = SortBuilder.newBuilder()
                .sortStrategy(SortStrategies.QUICK)
                .pivotStrategy(PivotStrategies.MIDDLE_ELEMENT)
                .build(new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2) {
                        if (o1.compareTo(o2) == 0) return 0;
                        return o1.compareTo(o2) > 0 ? 1 : -1;
                    }
                });

        List<String> testList = new ArrayList<>(unsortedList);
        assertEquals(sortedList, testSorter.sort(testList));
    }

    @Test
    public void testBuildImmutable() {
        Sorter<String> testSorter = SortBuilder.newBuilder()
                .sortStrategy(SortStrategies.QUICK)
                .pivotStrategy(PivotStrategies.MIDDLE_ELEMENT)
                .buildImmutable(new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2) {
                        if (o1.compareTo(o2) == 0) return 0;
                        return o1.compareTo(o2) > 0 ? 1 : -1;
                    }
                });

        List<String> testList = new ArrayList<>(unsortedList);
        testSorter.sort(testList);
        assertEquals(unsortedList, testList);
        assertEquals(sortedList, testSorter.sort(testList));
    }
}
