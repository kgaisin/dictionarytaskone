package com.gaisinkv.pivotStrategies;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class FirstElementPivotStrategyTest {

    @Test
    public void testFirstElementPivot() {

        FirstElementPivotStrategy firstElementPivotTest = new FirstElementPivotStrategy();
        int startIndex = 1;
        int endIndex = 5;
        int pivot = firstElementPivotTest.getPivot(startIndex, endIndex);
        assertEquals(startIndex, pivot);
    }
}