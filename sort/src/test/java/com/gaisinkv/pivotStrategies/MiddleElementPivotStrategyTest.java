package com.gaisinkv.pivotStrategies;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class MiddleElementPivotStrategyTest {

    @Test
    public void testMiddleElementPivot() {
        MiddleElementPivotStrategy testMiddleElementPivot = new MiddleElementPivotStrategy();
        int startIndex = 0;
        int endIndex = 5;
        int middleIndex = startIndex - (startIndex - endIndex) / 2;
        int testPivot = testMiddleElementPivot.getPivot(startIndex, endIndex);
        assertEquals(middleIndex, testPivot);
    }
}