package com.gaisinkv.pivotStrategies;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class LastElementPivotStrategyTest {

    @Test
    public void testLastElementPivot() {
        LastElementPivotStrategy lastElementPivotTest = new LastElementPivotStrategy();
        int startIndex = 0;
        int endIndex = 5;
        int testPivot = lastElementPivotTest.getPivot(startIndex, endIndex);
        assertEquals(endIndex, testPivot);
    }
}