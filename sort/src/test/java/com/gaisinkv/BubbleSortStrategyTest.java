package com.gaisinkv;

import com.gaisinkv.exceptions.ArrayIsEmptyException;
import com.gaisinkv.sortStrategies.BubbleSortStrategy;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.*;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class BubbleSortStrategyTest {

    private BubbleSortStrategy<String> bubbleSorter = new BubbleSortStrategy<>();
    private String[] unsortedLetters = {"cc", "a", "za", "zz"};
    private List<String> unsortedLettersList = Arrays.asList(unsortedLetters);
    private String[] sortedLetters = Arrays.copyOf(unsortedLetters, unsortedLetters.length);
    private List<String> sortedLettersList = Arrays.asList(sortedLetters);
    { Collections.sort(sortedLettersList); }

    @Test
    public void testSortingExample() {
        List<String> testList = new ArrayList<>(unsortedLettersList);
        bubbleSorter.sort(testList, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (o1.compareTo(o2) == 0) return 0;
                return o1.compareTo(o2) > 0 ? 1 : -1;
            }
        });
        assertEquals(sortedLettersList, testList);
    }

    @Test
    public void testArrayLength() {
        List<String> testList = new ArrayList<>(unsortedLettersList);
        bubbleSorter.sort(testList, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (o1.compareTo(o2) == 0) return 0;
                return o1.compareTo(o2) > 0 ? 1 : -1;
            }
        });
        assertEquals(sortedLettersList.size(), testList.size());
    }

    @Test(expected = NullPointerException.class)
    public void testArrayIfArgIsNull() {
        bubbleSorter.sort(null, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (o1.compareTo(o2) == 0) return 0;
                return o1.compareTo(o2) > 0 ? 1 : -1;
            }
        });
    }

    @Test(expected = ArrayIsEmptyException.class)
    public void testExceptionIfArrayIsEmpty() {
        List<String> testList = new ArrayList<>();
        bubbleSorter.sort(testList, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (o1.compareTo(o2) == 0) return 0;
                return o1.compareTo(o2) > 0 ? 1 : -1;
            }
        });
    }
}
